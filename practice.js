function findPlatform(arr, dep, n)
    {
        arr = arr.sort();
        dep = dep.sort();

        let plat_needed = 1;
        let result = 1;
        let i = 1;
        let j = 0;

        while(i < n && j < n)
        {
            if(arr[i] <= dep[j])
            {
                plat_needed++;
                i++;
            }
            else if (arr[i] > dep[j])
            {
                plat_needed -- ;
                j++;
            }

            if(plat_needed > result)
            result = plat_needed;
        }
        return result;
    }
    let arr = new Array(1300,1400,1530,1700);
    let dep = new Array(1330,1410,1800,1830);
    let n = arr.length;
    console.log("Minimum number of platform required = " + findPlatform(arr, dep, n));

